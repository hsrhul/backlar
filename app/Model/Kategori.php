<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table    = 'kategori';
    protected $primaryKey = 'kategoriid';
    // public $incrementing = false;
    protected $fillable = [
        'kategoriid',
        'kategori'
    ];
}

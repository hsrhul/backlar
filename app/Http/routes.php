<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::auth();

// Route::get('/home', 'HomeController@index');

Route::group(['middleware' => ['web']], function () {
    Route::auth();
    Route::get('/', 'HomeController@index');

    // Route::get('wel', function () {
    //     return view('welcome');
    // })->middleware('isAdmin');

    // Route::get('unit', 'UnitController@index')->middleware('auth');
    // Route::get('/unit/create', 'UnitController@create')->middleware('auth');
    // Route::post('/unit/store', 'UnitController@store')->middleware('auth');
    // Route::get('/unit/edit/{id}', 'UnitController@edit')->middleware('auth');
    // Route::post('/unit/update/{id}', 'UnitController@update')->middleware('auth');
    // Route::get('/unit/delete/{id}', 'UnitController@destroy')->middleware('auth');
  
    Route::resource('kategori', 'kategoriController');
    
    // Route::resource('modulegroup', 'ModulegroupController');
    // Route::resource('module', 'ModuleController'); 


 
});

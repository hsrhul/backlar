<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect; 

use App\Http\Requests;
use App\Model\Kategori; 
use Input; 

class KategoriController extends Controller
{
    public function index()
    { 
        // $model = Kategori::select('*')->get();
        // return View::make('kategori.index', compact('model'));  
        $model = Kategori::select('*')->orderBy('kategoriid')->paginate(10);
        return view('kategori.index', compact('model'));
    }

    public function create()
    {  
        return View::make('kategori.create');
    }
	
	public function store()
    { 
            $model = new Kategori;
            $model->kategori 		= Input::get('kategori'); 
            $model->save(); 
            return Redirect::to('kategori')->with('msg', 'Input success ..!');; 
    }

    public function edit($id)
    {
        $model = Kategori::find($id);   
        return View::make('kategori.edit', compact('model'));
    }

    public function delete($id)
    { 
        $model = Kategori::find($id);  
        return View::make('kategori.delete', compact('model'));
    }

    public function update($id)
    {
		$model = Kategori::find($id);
        $model->kategori 		= Input::get('kategori');  
        $model->save();
        return Redirect::to('kategori')->with('msg', 'Update success ..!');; 
    } 

    public function destroy($id)
    { 
        $nerd = Kategori::find($id);
        $nerd->delete(); 
        return Redirect::to('kategori');
    }
}

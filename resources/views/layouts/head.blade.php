    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Web Application</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('temp/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('temp/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('temp/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('temp/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('temp/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('temp/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('temp/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('temp/assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->



    <!-- form -->     

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('temp/assets/js/core/libraries/jasny_bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/plugins/forms/inputs/autosize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/plugins/forms/inputs/formatter.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/plugins/forms/inputs/typeahead/handlebars.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/plugins/forms/inputs/passy.js') }}"></script>
    <script type="text/javascript" src="{{ asset('temp/assets/js/plugins/forms/inputs/maxlength.min.js') }}"></script>
 
    <script type="text/javascript" src="{{ asset('temp/assets/js/pages/form_controls_extended.js') }}"></script>
    <!-- /theme JS files -->
 
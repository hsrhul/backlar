@extends('layouts.dashboard')

@section('content')  


          <!-- Masked inputs -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Form Kategori</h5>
                    <!-- <hr> -->
                    <!-- <h5 class="panel-title">Form Kategori</h5> -->
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="reload"></a></li> 
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        
                        {!!Form::open(array('url' => 'kategori'))!!}

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kategori </label>
                                <input type="text" class="form-control" name="kategori"> 
                            </div> 
                        </div>

                        <!-- <div class="col-md-4">
                            <div class="form-group">
                                <label>International format: </label>
                                <input type="text" class="form-control" data-mask="+39 999 999 999" placeholder="Enter your phone in international format"> 
                            </div> 
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Product key: </label>
                                <input type="text" class="form-control" data-mask="a*-999-a999" placeholder="Enter your product key"> 
                            </div> 
                        </div> -->
                        <div class="col-md-12">
                            <div class="form-group">  
                                <button type="submit" class="btn btn-primary pull-right">Create</button>
                            </div> 
                        </div>             
                        
                        {!!Form::close()!!}
 

                    </div>  
                </div>
            </div>
            <!-- /masked inputs -->

@endsection
 

    

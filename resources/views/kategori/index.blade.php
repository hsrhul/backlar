@extends('layouts.dashboard')

@section('content')
        
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Framed bordered</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="reload"></a></li> 
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
            
            <div class="panel-body">
                <a href="{{ url('/kategori/create') }}" class="btn btn-xs btn-danger"> <i class="fa fa-plus"></i> &nbsp; Add New </a> 
            </div>
            
            <div class="panel-body"> 

                <div class="table-responsive">
                    <table class="table table-bordered table-framed">
                        <thead>
                            <tr> 
                                <th>ID.kategori</th> 
                                <th>Kategori</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($model as $d)
                                <tr>
                                    <td> {{ $d->kategoriid }} </td> 
                                    <td> {{ $d->kategori }} </td> 
                                    <td> 
                                                  
                                        <div class="btn-group">   
                                            <a href="{{ URL::to('kategori/' . $d->kategoriid . '/edit') }}" class="btn btn-info pull-right btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
                                        </div>

                                        <div class="btn-group">
                                           {{ Form::open(array('url' => 'kategori/' . $d->kategoriid, 'class' => 'pull-right' )) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button> 
                                        {{ Form::close() }}   
                                        </div>
                                                  

                                    </td>
                                </tr>  
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class=" text-center">                     
                {!! $model->links() !!}
            </div>
        </div>
  
@endsection
 

    
